#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef PACKAGE
#undef VERSION
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_GMP2_INCLUDE_DIR
#undef HAVE_LIBSM
#undef HAVE_PROGRAM_INVOCATION_NAME
#undef HAVE_PROGRAM_INVOCATION_SHORT_NAME
#undef HAVE_SHADOW_H
#undef HAVE_GLIBTOP_MACHINE_H
#undef HAVE_LIBGTOP
#undef NEED_LIBGTOP
#undef HAVE_GUILE
#undef HAVE_LINUX_TABLE

/* Define if LibGTop has support for multiple processors. */
#undef HAVE_LIBGTOP_SMP

/* Define if you want to build against development gtk */
#undef HAVE_DEVGTK

/* Define if there is no `u_int64_t' and `int64_t'. */
#undef u_int64_t
#undef int64_t
