/* 
 * Piggie
 */

#include "config.h"
#include <gtk/gtk.h>

#define _(x) x

#include <stdlib.h>
#include <time.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

typedef struct _Page Page;
struct _Page {
	gboolean (* check) (void); /* function to call when moving to the next
				      page to check current input, return
				      FALSE if input on page is OK */
	int (* next_page) (void); /* returns the number of pages to skip
				     till next page */
	int (* prev_page) (void); /* returns the number of pages to skip till
				     previous page */

	gboolean finish_page; /* is this a finish page */

	void (* finish) (void); /* the finisher code */
};

static int curpage = -1;
static GList *pages = NULL;
static Page *curpage_page = NULL;

static GtkWidget *notebook = NULL;
static GtkWidget *next_button = NULL;
static GtkWidget *prev_button = NULL;
static GtkWidget *finish_button = NULL;

static int
one (void)
{
	return 1;
}

static int
zero (void)
{
	return 0;
}

static void
add_new_page (GtkWidget *page,
	      gboolean (* check) (void),
	      int (* next_page) (void),
	      int (* prev_page) (void),
	      gboolean finish_page,
	      void (* finish) (void))
{
	Page *pg = g_new0 (Page, 1);
	pg->check = check;
	pg->next_page = next_page;
	pg->prev_page = prev_page;
	pg->finish_page = finish_page;
	pg->finish = finish;

	pages = g_list_append (pages, pg);

	gtk_widget_show (page);

	gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
				  page,
				  NULL);
}

static void
add_greeting_page (void)
{
	add_new_page (gtk_label_new (_("Intro blah blah blah blah blah blah\n"
				       "blah blah blah blah blah blah\n"
				       "blah blah blah blah blah blah\n"
				       "blah blah blah blah blah blah\n"
				       "blah blah blah blah blah blah\n"
				       "blah blah blah blah blah blah\n"
				       "blah blah blah blah blah blah")),
		      NULL /* check */,
		      one /* next_page */,
		      zero /* prev_page */,
		      FALSE /* finish_page */,
		      NULL /* finish */);
}

static void
add_finish_page (void)
{
	add_new_page (gtk_label_new (_("Finish, bleh blah blah blah blah blah\n"
				       "blah blah bleh blah blah blah\n"
				       "blah blah bleh blah blah blah\n"
				       "blah blah bleh blah blah blah\n"
				       "blah blah bleh blah blah blah\n"
				       "blah blah bleh blah blah blah\n"
				       "blah blah bleh blah blah blah")),
		      NULL /* check */,
		      zero /* next_page */,
		      one /* prev_page */,
		      TRUE /* finish_page */,
		      gtk_main_quit /* finish */);
}



static void
set_page (int page)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (notebook),
			       page);
	curpage = page;

	curpage_page = g_list_nth_data (pages, page);

	g_assert (curpage_page != NULL);

	if (curpage_page->finish_page) {
		gtk_widget_show (finish_button);
		gtk_widget_hide (next_button);
	} else {
		gtk_widget_hide (finish_button);
		gtk_widget_show (next_button);
	}

	if (curpage_page->prev_page () == 0)
		gtk_widget_set_sensitive (prev_button, FALSE);
	else
		gtk_widget_set_sensitive (prev_button, TRUE);
	if (curpage_page->next_page () == 0)
		gtk_widget_set_sensitive (next_button, FALSE);
	else
		gtk_widget_set_sensitive (next_button, TRUE);
}

static void
previous_cb (GtkWidget *w, gpointer data)
{
	g_assert (curpage_page != NULL);
	g_assert (curpage_page->prev_page != NULL);

	/* no value sanity check for previous page */

	set_page (curpage - curpage_page->prev_page ());
}

static void
next_cb (GtkWidget *w, gpointer data)
{
	g_assert (curpage_page != NULL);
	g_assert (curpage_page->next_page != NULL);

	if (curpage_page->check != NULL &&
	    ! curpage_page->check ())
		return;

	set_page (curpage + curpage_page->next_page ());
}

static void
finish_cb (GtkWidget *w, gpointer data)
{
	g_assert (curpage_page != NULL);
	g_assert (curpage_page->finish_page);
	g_assert (curpage_page->finish != NULL);

	if (curpage_page->check != NULL &&
	    ! curpage_page->check ())
		return;

	curpage_page->finish ();
}

struct {
	void (*append_page) ();
} page_templates[] = {
	{ add_greeting_page },
	{ add_finish_page },
	{ NULL }
};

static GtkWidget *
make_install_dialog (void)
{
	GtkWidget *dialog;
	GtkWidget *button;
	int i;

	dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (dialog), _("Plain-GNOME Installer"));

	/* the notebook to put different pages of the "druid" in */
	notebook = gtk_notebook_new ();
	gtk_widget_show (notebook);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), FALSE);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), notebook,
			    TRUE, TRUE, 0);

	button = gtk_button_new_with_label (_("Cancel"));
	gtk_widget_show (button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (gtk_main_quit),
			    NULL);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			    button, FALSE, FALSE, 0);

	prev_button = gtk_button_new_with_label (_("Previous"));
	gtk_widget_show (prev_button);
	gtk_signal_connect (GTK_OBJECT (prev_button), "clicked",
			    GTK_SIGNAL_FUNC (previous_cb),
			    NULL);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			    prev_button, FALSE, FALSE, 0);

	next_button = gtk_button_new_with_label (_("Next"));
	gtk_widget_show (next_button);
	gtk_signal_connect (GTK_OBJECT (next_button), "clicked",
			    GTK_SIGNAL_FUNC (next_cb),
			    NULL);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			    next_button, FALSE, FALSE, 0);

	finish_button = gtk_button_new_with_label (_("Finish"));
	gtk_widget_show (finish_button);
	gtk_signal_connect (GTK_OBJECT (finish_button), "clicked",
			    GTK_SIGNAL_FUNC (finish_cb),
			    NULL);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			    finish_button, FALSE, FALSE, 0);

	for (i = 0; page_templates[i].append_page != NULL; i++) {
		page_templates[i].append_page ();
	}

	set_page (0);

	return dialog;
}

int
main (int argc, char **argv)
{
	GtkWidget *dialog;

	/*bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);*/
	
	gtk_init (&argc, &argv);

	dialog = make_install_dialog ();

	gtk_widget_show (dialog);

	gtk_main ();

	return 0;
}
